json.extract! bebida, :id, :nome, :preco, :litro, :categoria, :pais, :descricao, :created_at, :updated_at
json.url bebida_url(bebida, format: :json)