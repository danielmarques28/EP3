class AddAttachmentAvatarToBebidas < ActiveRecord::Migration
  def self.up
    change_table :bebidas do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :bebidas, :avatar
  end
end
